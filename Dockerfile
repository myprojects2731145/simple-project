FROM node:20.8.0
COPY . /app
RUN cd /app
RUN apt update -y 
EXPOSE 5000
CMD ["node", "/app/app.js"]
